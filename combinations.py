#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 25 10:48:35 2019

@author: william
"""
def _data_increment(data):
    _1 = [1] + [0]*(len(data))
    for i in range(len(data)):
        _aux = data[i] + _1[i]
        if _aux == 1:
            data[i] = _aux%2
        elif _aux == 2:
            data[i] = _aux%2
            _1[i+1] = 1
    return data
            

def get_combinations(arr, r):
    data = [1]+[0]*(len(arr)-1)
    combinations = []
    while sum(data) != 0:
        _data_increment(data)
        if sum(data) == r:
            _value = []
            for i, d in enumerate(data):
                if d:
                    _value.append(arr[i])
            combinations.append(_value)
    
    return combinations


if __name__=="__main__":
    arr = [(0,1),
           (1,1),
           (1,2),
           (0,1),
           (2,3),
           (2,3),
           (4,3),
           (5,3),
           (2,2),
           (2,7),
           (3,3)]
    arr = list(set(arr))
    r = 4
    combinations = get_combinations(arr, r)
    for c in combinations:
        print(c)
        break