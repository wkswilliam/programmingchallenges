#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 23 21:36:03 2019

@author: william
"""
import numpy as np

def get_slope(point_1, point_2):
    _x_1 = point_1[0]
    _x_2 = point_2[0]
    _y_1 = point_1[1]
    _y_2 = point_2[1]
    _delta_y = _y_1 - _y_2
    _delta_x = _x_1 - _x_2
    
    if _delta_x == 0:
        return np.inf
    else:
        return (_delta_y/_delta_x)

def get_y_intercept(point_1, point_2):
    _x_1 = point_1[0]
    _x_2 = point_2[0]
    _y_1 = point_1[1]
    _y_2 = point_2[1]
    
    _slope = get_slope(point_1, point_2)
    
    if _slope == np.inf:
        return _x_1
    else:
        return ((_y_1 + _y_2)-_slope*(_x_1 + _x_2))/2
    
def get_line_parameters(point_1, point_2):
    _s = get_slope(point_1, point_2)
    _y = get_y_intercept(point_1, point_2)
    
    return (_s, _y)

points = [(0,0),
          (0,1),
          (1,0),
          (1,1)]

lines = set()

for i, p_1 in enumerate(points):
    p_2 = points[i+1]
    p_3 = points[i+2]
    p_4 = points[i+3]
    print(p_1, p_2, p_3, p_4)
    if i+4 == len(points):
        break
            
            
            
            
            